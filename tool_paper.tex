\documentclass{sig-alternate-per}
\input{macros.tex}

\usepackage[colorlinks=true]{hyperref}

\begin{document}

\conferenceinfo{TOSME 2021}{Online Conference}


\title{rmf\_tool -- A library to Compute (Refined) Mean Field Approximation(s)}
% \subtitle{[Extended Abstract]

\numberofauthors{2} 
\author{
\alignauthor
Sebastian Allmeier\\
\affaddr{Univ. Grenoble Alpes}\\
\affaddr{Inria}\\
\affaddr{Grenoble, France}\\
\email{sebastian.allmeier@inria.fr}
\alignauthor
Nicolas Gast\\
\affaddr{Univ. Grenoble Alpes}\\
\affaddr{Inria}\\
\affaddr{Grenoble, France}\\
\email{nicolas.gast@inria.fr}
}

\date{\today}
% Just remember to make sure that the TOTAL number of authors
% is the number that will appear on the first page PLUS the
% number that will appear in the \additionalauthors section.

\maketitle

\begin{abstract}
       Mean field approximation is a powerful technique to study the performance of large stochastic systems represented as systems of interacting objects. Applications include load balancing models, epidemic spreading, cache replacement policies, or large-scale data centers, for which mean field approximation gives very accurate estimates of the transient or steady-state behaviors. In a series of recent papers \cite{gastRefinedMeanField2017,gastSizeExpansionsMean2019}, a new and more accurate approximation, called the \emph{refined} mean field approximation is presented. Yet, computing this new approximation can be cumbersome. The purpose of this paper is to present a tool, called \rmftool, that takes the description of a mean field model, and can numerically compute its mean field approximations and refinement. %We briefly discuss implementation design and present a few examples of use.
\end{abstract}

\section{Introduction}

Mean field approximation is widely applied to analyze the behavior of large stochastic systems. It applies to systems composed of $n$ interacting objects. The idea of the approximation is to consider that objects within the system evolve independently. This transforms the study of a multi-dimensional stochastic process into much smaller stochastic processes that are weakly coupled. Under mild conditions, the mean field approximation is described by a finite set of deterministic ordinary differential equations (ODEs). As such, it can be simulated at low computational cost. Mean field approximation finds widespread use in fields such as epidemic spreading \cite{montalbanHerdImmunityIndividual2020,dearrudaFundamentalsSpreadingProcesses2018}, load balancing strategies \cite{mukhopadhyayAnalysisLoadBalancing2015,mitzenmacherPowerTwoChoices2001}, the study of cache replacement strategies \cite{gastTransientSteadystateRegime2015} or SSDs \cite{vanhoudtMeanFieldModel2013}.

Classical models to which mean field approximation applies are the class of density dependent population processes (DDPPs, \cite{kurtz1978strong}), whose definition is recalled in Section~\ref{sec:model} -- epidemic spreading or load balancing models are typical examples of DDPPs. If $X$ is a density dependent population process in $d$ dimensions, its mean field approximation is the solution of a system of non-linear ODEs $\dot{x}=f(x)$ where $f:\R^d\to\R^d$ is called the drift of the system. Computing the mean field approximation can be easily automated, as the drift $f$ can be expressed easily from the model's definition. Our tool incorporates this but, more importantly, allows going further.


Building on mean field approximation, the authors of \cite{gastRefinedMeanField2018,gastSizeExpansionsMean2019} introduce the notion of \emph{refined} mean field approximation. This approximation consists in adding an expansion term to the original approximation. Denoting by $x$ the value of the mean field approximation, it is shown in \cite{gast2017expected} that there exists a deterministic quantity $v(t)$ such that:
\begin{align*}
       \esp{X(t)} &= \underbrace{x(t) + \frac1n v(t)}_{\text{refined m.f. approx.}} + O(\frac1{n^2}).
\end{align*}
The quantity $v(t)$ is the solution of a time-inhomogeneous linear ODE. As shown in the aforementioned papers, the construction of this set of ODEs is direct from the model description but involves computing the derivatives of the drift, which can be cumbersome. 

The purpose of \rmftool{} -- the refined mean field tool -- is to make mean field and refined mean field approximation easily computable. Our tool is composed of a Python library. The tool takes as input a description of the system, which can be either a density dependent population process or a heterogeneous population model, and can be used to compute the mean field and refined mean field approximations numerically. The tool relies on standard libraries (like \texttt{numpy} and \texttt{scipy}) to construct and solve the corresponding ODEs. The tool is provided with a series of examples to demonstrate its expressiveness and the accuracy of the various approximations.


\paragraph*{Related tools} There exist a large number of tools that provide methods to construct and simulate stochastic population models. Yet, to the best of our knowledge, the only tool that provides a way to analyze size expansion methods (which are essentially equivalent to our refined mean field approximation) is the iNA software of \cite{thomas2012intrinsic}.  The iNA is a complete simulation toolbox (that includes its own graphical interface). Compared to this software, we use a more lightweight approach by providing a pure python library that can be easily integrated. 

\paragraph*{Roadmap} The paper is centered around the tool. We first describe the set of models to which the tool applies in Section~\ref{sec:model}, along with examples on how they can be defined within the tool. We then describe what are the mean field and refined mean field approximation, and how one can use the tool to compute them in Section~\ref{sec:mean-field}.  We detail some technical challenges in Section~\ref{sec:challenges} and conclude in Section~\ref{sec:conclusion}.

\paragraph*{Reproducibility} Our tool is provided as an open-source software at \url{https://github.com/ngast/rmf_tool}. The code to reproduce the current paper along with all figures is available at \url{https://gitlab.inria.fr/gast/toolpaper_rmf}.

\section{Models}
\label{sec:model}


The tool that we develop accepts three kinds of models: homogeneous population processes (HomPP), density dependent population processes (DDPPs) and heterogeneous population models (HetPP). First, we describe the notion of the HomPP of which DDPP and HetPP are generalizations. 

\subsection{Homogeneous population process}
\label{sec:homogeneous}

Population Processes are widely used to describe the evolution of a number of interacting objects (or individuals). A homogeneous population model consists of $n$ interacting objects that each evolves in a finite state space $\{1\hdots d\}$. All objects have similar transition rates which are a combination of unilateral and pairwise interactions, i.e. objects can change their state with or without interacting with one other member of the population. Let $X_s(t)$ be the fraction of objects that are in state $s$ at time $t$. We assume that $\bX=(X_1\dots X_d)$ is a continuous time Markov chain whose transitions are such that for all state $s,s',\tilde{s},\tilde{s'}$:
\begin{itemize}
       \item[(Uni.)] An object in state $s$ moves to state $s'$ at rate $a_{s,s'}$.
       \item[(Pair.)] A pair of objects in state $(s,\tilde{s})$ moves to state $s',\tilde{s}'$ at rate $b_{s,\tilde{s},s',\tilde{s}'}/n$.
\end{itemize}
Note that for pairwise interactions, the rate is scaled by $1/n$ as the number of pairs of objects is $n$ times larger than the number of objects.

\paragraph*{Example: the SIS model} One of the simplest examples of population process is the epidemic model called the SIS model. In an SIS model, each object can be in one of the two states $S$ (susceptible) or $I$ (infected). Susceptible objects can become infected from an external source (unilateral transition) or when meeting an infected individual (pairwise transition). An infected individual can recover and become susceptible again (unilateral transition). We assume that an individual becomes infected at rate $\alpha$ by an external source, and recovers at rate $\beta$. Moreover, assume that the rate at which two individuals meet is $\gamma/n$ and that when a susceptible meets an infected individual, the susceptible gets infected. 


With our tool, we define a class called HomPP for which we specify the transition rates and an initial state. For the SIS model above, with $\alpha, \beta, \gamma = 1$, we write:
\begin{python}
import rmf_tool as rmf
model = rmf.HomPP()
d, S, I = 2, 0, 1
A, B = np.zeros((d, d)), np.zeros((d, d, d, d)) 
A[S, I] = 1                 # \alpha
A[I, S] = 1                 # \beta
B[S, I, I, I] = 1           # \gamma
model.add_rate_tensors(A, B)
\end{python}
The specified model can be used to simulate stochastic trajectories of the underlying process for various population sizes. It can also be used to compute the mean field approximation and the refinements (see Section~\ref{sec:mean-field}). For instance, if one wants to simulate a trajectory for a population of size $n=1000$, where all individuals are susceptible in the initial state, one would write:
\begin{python}
model.set_initial_state([1,0])
t, X = model.simulate(N=1000, time=2)
\end{python}

\paragraph*{State representation}

Recall that $X_s(t)$ is the fraction of objects in state $s$ at time $t$. The transitions of such a model can be expressed\footnote{The notation $\be_{s}\in \{0,1\}^d$ correspond to a vector of size $d$ whose $s$ entry is equal to $1$, all others being $0$.} as:
\begin{itemize}
       \item[(Uni.)] When an object moves from $s$ to $s'$, this changes $\bX$ into $\bX+\frac1n(\be_{s'}-\be_s)$. As $nX_s(t)$ is the number of objects in state $s$, this transition occurs at rate $n a_{s,s'}X_s(t)$. 
       \item[(Pair.)] When a pair moves from $(s,\tilde{s})$ to $(s',\tilde{s}')$, this changes $\bX$ into $\bX+\frac1n(\be_{s'}+\be_{\tilde{s}'}-\be_{\tilde{s}}-\be_s)$. This transition occurs at rate $n b_{s,\tilde{s},s',\tilde{s}'}X_s(t)X_{\tilde{s}(t)}$.
\end{itemize}
Written in a compact way, those transitions are:
\begin{align}
       \bx&\to\bx+\frac1n(\be_{s'}-\be_s)& \text{ at rate $n a_{s,s'}x_s$}\label{eq:uni}\\
       \bx&\to\bx+\frac1n(\be_{s'}{+}\be_{\tilde{s}'}{-}\be_{\tilde{s}}{-}\be_s)&\text{ at rate $nb_{s,\tilde{s},s',\tilde{s}'}x_sx_{\tilde{s}}$}.\label{eq:pair}
\end{align}


\subsection{Density dependent populations process}

The class of homogeneous population model that we define is a subclass of density dependent population processes (DDPPs) that are introduced by Kurtz in the 70s \cite{kurtz1978strong}. For a given $n$, a DDPP defines a stochastic process $\bX\in\R^d$. The transitions of the process are specified by a finite set of vectors $\calL\subset\R^d$, and a set of corresponding rate functions $\beta_\ell:\R^d\to\R^+$ for all $\ell\in\calL$. The process $\bX$ jumps from $\bx$ to $\bx+\ell/n$ at rate $n\beta_\ell(\bx)$. 

It should be clear from Equation~\eqref{eq:uni}-\eqref{eq:pair} that HomPP is a special case of DDPP. DDPPs generalize HomPP since they allow to choose arbitrary transition rates as opposed to combinations of unilateral and pairwise transition. In the case where $nX_s(t)$ denotes the number of individuals in state $s$ at time $t$, the vector $\ell\in\calL$ indicates how many individuals are created or destroyed by a transition. For instance, if $d=3$, $\ell=(1, -1, 0)$ corresponds to having one additional individual in state $1$ and one less in state $2$ (this occurs typically when one individual moves from state $2$ to state $1$), $\ell=(0, 0, 2)$ corresponds to the creation of two additional individuals in state $3$. 


\paragraph*{The SIS model as a DDPP} To illustrate the relation between DDPPs and HomPP, consider the SIS model defined in the previous section and recall that $(X_S(t),X_I(t))$ is the fraction of susceptible and of infected individuals. The transitions $\ell\in\calL$ and their corresponding rates $\beta_\ell$ are:
\begin{center}
       \begin{tabular}{|c|c|c|}
              \hline
              Event & Transition $\ell$ & Rate $\beta_\ell(\bx)$\\
              \hline
              infection from ext. source & (-1,1) & $\alpha \bx_S$\\
              recovery & (1,-1) & $\beta \bx_I$\\
              infection from a meeting & (-1,1) & $\gamma \bx_S \bx_I$\\
              \hline
       \end{tabular}
\end{center}


Within our tool, we define a class called \texttt{DDPP} that can be used to define DDPPs directly from their mathematical definition. For the above SIS example, we would write: 
\begin{python}
import rmf_tool as rmf
model = rmf.DDPP()
alpha, beta, gamma = 1,1,1
model.add_transition([-1,1], lambda x: alpha*x[0])
model.add_transition([1,-1], lambda x: beta*x[1])
model.add_transition([-1,1], lambda x: gamma*x[0]*x[1])
\end{python}

As for the HomPP, the model can then be used to simulate the stochastic process, to compute the mean field approximation and the refinements. The syntax is identical. If one wants to run a simulation with a population of $n=1000$ where at the beginning all individuals are in the first state (susceptible), one would write:
\begin{python}
model.set_initial_state([1,0])
t, X = model.simulate(N=1000, time=2)
\end{python}

\subsection{Heterogeneous population process}
\label{ssec:heterogeneous}

In \cite{allmeier2021mean}, the authors extend the notion of the HomPPs to deal with populations of heterogeneous objects. As before, the heterogeneous population model consists of $n$ interacting objects which each evolve in a finite state space $\{1\hdots d\}$. Each object has a specific transition rate which is a combination of unilateral or pairwise interactions. In contrast to the HomPP, transition rates are object dependent:
\begin{itemize}
       \item The object $k$ moves from state $s$ to state $s'$ at rate $a_{k,s,s'}$.
       \item The pair $(k,k')$ moves from states $(s,\tilde{s})$ to states $(s',\tilde{s}')$ at rate $b_{k,k',s,\tilde{s},s',\tilde{s}'}/n$.
\end{itemize}
Note that the difference between a homogeneous population process and a heterogeneous population process is that the rate tensors $a$ and $b$ depend on the object id $k$. As a result, the process $\bX=(X_1\dots X_d)$ where $X_s(t)$ is the fraction of objects in state $s$ is not a Markov process. Let the stochastic process $\bZ \in \{0,1\}^{n\times d}$ describe the evolution of the population where $Z_{k,s} = 1$ indicates that object $k$ is in state $s$ and $Z_{k,s} = 0$ if it is not. The process $\bZ$ is a Markov process whose transitions are:
\begin{align*}
       \bz &\mapsto \bz \shortminus \be_{k,s} + \be_{k,s'} &\text{ at rate $a_{k,s,s'}z_{k,s}$}\\
       \bz &\mapsto \bz {\shortminus} \be_{k,s} {+} \be_{k,s'} {\shortminus} \be_{\tilde{k},\tilde{s}} {+} \be_{\tilde{k},\tilde{s}'}
       &\text{at rate $\frac{1}{n}b_{k,\tilde{k},s,\tilde{s},s',\tilde{s}'}z_{k,s}z_{\tilde{k},\tilde{s}}$}.
\end{align*}
These transitions generalize \eqref{eq:uni}-\eqref{eq:pair}.   


\paragraph*{Example: Heterogeneous SIS model}

To set up a heterogeneous version of the previous SIS model we use the HetPP class of the toolbox. In contrast to the HomPP and DDPP class, the model can not be defined independent of the system size, \emph{i.e.}, $n$ and $d$ have to be defined to initialize the model. For instance, to set up a SIS model where objects are almost identical but some recover slower than others, we can use the code:
\begin{python}
import rmf_tool.src.heterogeneous_rmf_tool as hrmf
model = hrmf.HetPP()
N, d = 20, 2
S, I = 0, 1
A, B = np.zeros((N, d, d)), np.zeros((N, N, d, d, d, d))
A[:, S, I] = np.ones((N))
A[:, I, S] = np.random.rand(N)   # Hetero. recovery rates
B[:, :, S, I, I, I] = (1/N) * np.ones((N, N))
model.add_rate_tensors(A, B)
\end{python}
Here, the tensor A and B specify the transition rates where A[k,s,s'] $= a_{k,s,s'}$ and B[k,$\tilde{k}$,s,$\tilde{s}$,s',$\tilde{s}$'] $= \frac{1}{n} b_{k,\tilde{k},s,\tilde{s},s',\tilde{s}'}$. The corresponding transition vectors of the model are derived from the non zero rates of the tensors. The methods of the HetPP class are coherent to the HomPP and DDPP class.




\section{Mean field approximations and refinements}
\label{sec:mean-field}

\subsection{Mean field approximation (homogeneous)}

For a given DDPP, and a given state $\bx\in\R^d$, we define the drift in state $\bx$ as
\begin{align*}
       f(\bx) = \sum_{\ell\in\calL} \ell \beta_\ell(\bx).
\end{align*}
The drift corresponds to the average variation of the model, as it is the sum of state changes ($\ell$) weighted by the rate at which these changes occur. 

For a given initial condition $\bx(0)$, the mean field approximation of a DDPP is the solution of the ODE: 
\begin{align*}
       \dot{\bx}=f(\bx).
\end{align*}
The same holds true for any HomPP since the class of DDPP is essentially a scaled generalization of the former. Thus, all methods which are available for DDPPs are available for HomPP as well. Within our tool, the mean field approximation can be easily computed with:
\begin{python}
t, X = model.ode(time=2)
\end{python}
It is known from \cite{kurtz1978strong} that under very general conditions (essentially that $f$ is Lipschitz-continuous), the stochastic trajectories of $\bX$ converge to the mean field approximation $\bx$ as the scaling parameter $n$ goes to infinity. We illustrate the accuracy of the mean field approximation in Figure~\ref{fig:SIS_mf}, where we compare two stochastic trajectories of the system for populations of $n=100$ and $n=1000$ individuals, with the mean field approximation. 
\begin{figure}[ht]
       \centering
       \includegraphics[width=0.8\linewidth]{SIS_mf}
       \caption{Example: Simulation of the SIS (DDPP model)}
       \label{fig:SIS_mf}
\end{figure}

\subsection{The refined mean field approximation}

It is shown in \cite{gastRefinedMeanField2018,gastSizeExpansionsMean2019} that when the drift of the DDPP is twice differentiable, there exists a time varying vector $\bv$ and a time varying matrix $\bw$ such that:
\begin{align*}
       \esp{\bX(t)} &= \bx(t) + \frac1n \bv(t) + O(\frac{1}{n^2});\\
       \Var{\bX(t)} &= \frac1n \bw(t) + O(\frac{1}{n^2}),
\end{align*}
where $\Var{\bX(t)}$ is the covariance matrix of the stochastic process $\bX$.

The above equation holds for any finite time. It is shown in \cite{gastSizeExpansionsMean2019} that, for the transient regime, $\bv$ and $\bw$ satisfy a time-inhomogeneous linear ODEs. If there exists a point $\bx(\infty)$ such that for all initial condition $\bx(0)\in\R^d$, the solution of the ODE converges to $\bx(\infty)$ exponentially fast, then this equation holds uniformly in time and in particular is also true for the steady-state $t=+\infty$. In the latter case, the following linear equation (that is called a Lyapunov equation) is satisfied:
\begin{align}
       \label{eq:lyapunov}
       \bw J + J^T \bw + \bq=0,
\end{align}
where $J$ is the Jacobian of the drift $f$ evaluated at $\bx(\infty)$ and $\bq=\sum_\ell \ell\otimes\ell\beta_\ell(\bx(\infty))$. The vector $\bv(\infty)=J^{-1} (D\cdot \bw)$, where $D$ is the second derivative of $f$ evaluated at $\bx(\infty)$  and $\cdot$ denotes a tensor product: $(D\cdot \bw)_i=\sum_{jk}D_{i,jk}w_{jk}$ and $D_{i,jk}=(\partial^2 f/\partial x_j\partial x_k)$ evaluated in $x(\infty)$.

This means that they can be easily solved numerically. The tool provides methods to automatically compute these constants for the transient or the steady-state regime. These functions rely on \texttt{scipy}'s functions: for the transient regime it uses the \texttt{solve\_ivp} from \texttt{scipy} and for the steady-state the function \texttt{solve\_continuous\_lyapunov}. An example of the tool is:
\begin{python}
t, x, v, _ =\
       model.meanFieldExpansionTransient(order=1,time=2)
x_inf, v_inf,_ =\
       model.meanFieldExpansionSteadyState(order=1)
x_simu, _ = ddpp.steady_state_simulation(N=n, time=20000)
\end{python}
where the last line estimates $\esp{\bX(\infty)}$ by simulating a trajectory of $20000$ events and computes the average over the end of the trajectory. 

This result is illustrated in Table~\ref{table:SIS}, where we compare the mean field approximation, the refined mean field approximation and an estimation of the steady-state probability $\esp{X_s(\infty)}$ computed by simulation. We observe that if the mean field approximation is already very accurate, its refined version is close to being exact.
\begin{table}[ht]
       \begin{tabular}{|c|ccc|}
              \hline 
              $n$& M-f $\bx(\infty)$ & Refined $\bx(\infty)+\frac1n \bx(\infty)$ & Simulation \\
              \hline 
              10&0.382 & 0.394 & $0.394\pm0.004$\\
              20&0.382 & 0.388 & $0.389\pm0.003$\\
              30&0.382 & 0.386 & $0.386\pm0.002$\\              
              \hline
       \end{tabular}
       \caption{SIS model: Illustration of the accuracy of the mean field and refined mean field approximations for steady-state.}
       \label{table:SIS}
\end{table}

Note that the tool also allows to compute the second order refinement term as defined in \cite{gastSizeExpansionsMean2019}. This can be done by changing the \texttt{order=1} into \texttt{order=2} in the code. The time to compute this approximation is much larger than the time to compute the refined mean field approximation (that corresponds to a first order expansion). 

\subsection{Heterogeneous mean field approximation and refinements}

The heterogeneous mean field approximation and its refinement differs from the homogeneous case in the sense that transitions are dependent on the state of single objects. For the stochastic process this is taking into account by considering an object dependent representation. The intuition of the mean field approximation is as before, for the drift we consider the sum over all transitions weighted by their transition rate. Let the drift in state $z$ be denoted by $f^{het}(z)$, then, the mean field approximation is again the solution to the ode having $f^{het}$ as drift with initial condition $z(0)$. If both, $a_{k,s,s'}$ and $b_{k,\tilde{k},s,\tilde{s},s',\tilde{s}'}$ are uniformly bounded, it holds, as shown in \cite{allmeier2021mean} that the adapted mean field and refined mean field approximation capture the probability of the objects to be in a state with an accuracy of $O(1/n)$ and $O(1/n^2)$, i.e.
\begin{align}
& \E[Z_{k,s}(t)] = \mathbb{P}(Z_{k,s}(t) = 1) = z_{k,s}(t) + O(1/n), \label{eq:mf_hetero}\\
& \E[Z_{k,s}(t)] = \mathbb{P}(Z_{k,s}(t) = 1) = z_{k,s}(t) + v_{k,s}(t) + O(1/n^2). \nonumber
\end{align}
The term $v_{k,s}(t)$ refers to the adapted refinement term whose precise definition can be found in \cite[Appendix B]{allmeier2021mean}.


Simulations of stochastic trajectories, mean field and the refinement methods can be calculated by calling the same functions as for the homogeneous case. Note that second order refinement methods are note available for the current version since they are computationally too expensive.

Due to the setup of the heterogeneous population process, single simulation trajectories are not close to the mean field approximation but close to the sample mean of the stochastic system, that is, \eqref{eq:mf_hetero} holds but $Z_{k,s}(t)$ does not converge to $z_{k,s}(t)$ as $n$ goes to infinity (contrary to what appends to the DDPP case for which one can show \cite{kurtz1978strong} that $X_s(t)$ converges in probability to its mean field approximation $x_s(t)$, which is what is observed in Figure~\ref{fig:SIS_mf}). Hence, to study the accuracy of the mean field and refined mean field approximation in the heterogeneous context, we provide the additional methods \texttt{sampleMean}, \texttt{sampleMeanVariance} with which the sample mean and sample variance can be calculated. To calculate an approximated mean with 100 samples, we set the initial state to have only susceptible objects and write:
\begin{python}
model.set_initial_state(np.ones((N,d))*np.array([1,0]))
t_mean, mean, var =\ 
       model.sampleMeanVariance(time=2, samples=100)
\end{python}
In order to compare the results to a one of the homogeneous models one should consider the sum $Y_s(t) = \frac1n\sum_{k=1}^n Z_{(k,s)}(t)$, which is a density representation of the heterogeneous population process. It can be shown that $Y_s(t)$ converges in probability to its mean field approximation $y_s(t)$, as the number of objects grows.



\section{Implementation challenges}
\label{sec:challenges}

Most of the toolbox functionality is a direct implementation of the equations defined in \cite{gast2017refined,gastSizeExpansionsMean2019}, with the use of functions from \texttt{numpy} or \texttt{scipy} to integrate differential equations or solve linear equations. Yet, there are some implementation challenges among which we list two here: how to automatically compute the drift's derivatives (Section~\ref{ssec:diff}), and how to deal with model that do not satisfy the exact assumptions of \cite{gast2017refined} needed for the steady-state (Section~\ref{ssec:dimension_reduction}).

\subsection{Automatic differentiation}
\label{ssec:diff}

To compute the refined mean field approximation, one needs to compute the first and second derivatives of the drift function $f$. We implement three different methods. The first is to use a finite difference method. $\partial f_i(\bx)/\partial x_j\approx (f_i(\bx+\varepsilon \be_j)-f_i(\bx))/\varepsilon$. This is the most robust method but is relatively slow and has a limited precision due to the choice of $\varepsilon$. The second method that we implement is to use the package \texttt{simpy} that allows for symbolic computation and can be used to compute derivatives. The third method is a method based on \texttt{autograd} from \texttt{jax} that uses automatic differentiation. These two methods are both faster and more accurate than finite difference methods. Yet, they cannot differentiate all functions. For instance, if the drift involves a sinus function and if the DDPP model is defined using the \texttt{numpy.sin} function, then the \texttt{simpy} will not be able to differentiate this function as it does not understand the \texttt{numpy} function. Here, \texttt{autograd} will work. 

\subsection{Dimension reduction}
\label{ssec:dimension_reduction}

In order for the equation \eqref{eq:lyapunov} to have a unique solution, the assumption used in \cite{gast2017refined} is that all solutions of the mean field ODE $\dot{\bx}=f(\bx)$ converge to the same fixed point $\bx(\infty)$, regardless of the initial condition $\bx(0)\in\R^d$. Yet, in practice, many models are naturally described as $d$-dimensional DDPP but evolve in a smaller dimensional space $\calX\subset\R^d$. This is for instance the case for the SIS model of Section~\ref{sec:model} that evolves in a space of dimension $1$ because $x_S+x_I=1$. It further implies that the Lyapunov equation \eqref{eq:lyapunov} does not have a unique solution. As such, one cannot apply directly the theorem of \cite{gast2017refined} to this SIS model. 

A mathematical solution to this is to redefine our SIS model to obtain a model in dimension $1$. By replacing the occurrences of $x_I$ by $1-x_S$ in all equations. Yet, if this is easily done for reducing a 2D model to a 1D model, it can be cumbersome when going from a $20$D to a $15$D model. Our tool allows doing this automatically.  This is how we can obtain Table~\ref{table:SIS} while using the DDPP defined in Section~\ref{sec:model}. 

Our approach to this problem is to compute the rank of the set of transitions $\calL$. If this rank is $d'<d$, this means that the model evolves in a $d'$-dimensional state space. In particular, the jacobian $A$ used in Equation~\eqref{eq:lyapunov} has dimension at most $d'$. Our code uses the SVD decomposition of $A$ to transform the $d$-dimensional Lyapunov equation~\eqref{eq:lyapunov} into $d'$-dimensional equation. This is particularly useful for heterogeneous models composed of $n$ objects that each evolve in a $S$ dimensional state: a natural description of the model is to construct a $nS$-dimensional DDPP, but that evolves in a subset of dimension $n(S-1)$ or even smaller. For instance, the cache replacement policy studied in \cite{allmeier2021mean,casale2020performance,gastTransientSteadystateRegime2015} with $n$ objects and $S$ lists is naturally described as a $n(S+1)$ process but evolves in fact in a $nS - S$ state space. Using the automated dimension reduction greatly simplifies the definition of the model in the tool. 

\section{Conclusion and Discussion}
\label{sec:conclusion}

In this paper, we present a tool, called \rmftool, that can be used to define and study mean field interaction models. The tool is build-in with a stochastic simulator, and methods to compute the mean field approximation and refined approximation of a given model.  The tool consists on a Python library and models can be directly be defined as python objects.  In the present paper, we illustrate how the tool can be used by using a simple SIS model.  Below, we discuss in more detail the applicability of the tool by giving a few examples of application, and by analyzing the computation time.

\subsection{To which model does this apply?}

The tool is provided with a number of examples that demonstrates the use of the tool and the accuracy of the approximation. These examples include: 
\begin{itemize}
\item The power of two choice model of \cite{mitzenmacher1996power}. This example models a simple, yet powerful, load balancing strategy in a system composed of $n$ servers. 
\item The bike-sharing model of \cite{fricker2016incentives}.  It models a city where $Cn$ bikes moves in a city composed of $n$ stations. 
\item A epidemic model called the SIR model (that is a generalization of the SIS model presented in the paper). 
\end{itemize} 

Although they are not directly provided as examples in the repository, the tool is also used in \cite{allmeier2021mean,casale2020performance} to analyze the performance of cache replacement policies. These cache replacement policies are examples of non-homogeneous population models. 

\subsection{Analysis of the computation time}

To give an idea of the time needed to compute the refined approximation, we report in Figure~\ref{fig:computationTime} the time taken by the tool to compute the refined mean field approximation as a function of the system size. The first line corresponds to a homogeneous model of dimension $d$: in this model, we consider the power of two choice model of \cite{mitzenmacher1996power} where we bound the queue length by $d$. We report the numbers of \cite{gastSizeExpansionsMean2019}. We observe that for this model, we can solve the problem for a few hundreds of dimension in less than a few tens of seconds.  Note that for this example, the jacobian and the second derivative can be computed in close form. Hence, the reported time does not include the time that would be taken if one were to use symbolic differentiation. 

In the second line of Figure~\ref{fig:computationTime}, we report the time taken to solve the heterogeneous SIS model defined in Section~\ref{ssec:heterogeneous} with $n$ different objects. For this example, we use the \texttt{HetPP} class. Note that this class does not use symbolic differentiation since the derivative can be directly computed by using the $A$ and $B$ tensors. The model here is a $2n$ dimensional model. We observe that the time taken here for a model with $2n$ dimension is larger than the time for a homogeneous model of dimension $2n$. We believe that the run time could be improved by using sparse tensor multiplications and will consider this question for future work. 

\begin{figure}[ht]
       \centering
       \begin{tabular}{cc}
              \includegraphics[width=.435\linewidth]{figs/jsqD_computationTime_order1_transient}
              &\includegraphics[width=.45\linewidth]{figs/jsqD_computationTime_order1_steadyState}\\
              (a) Homogeneous transient & (b) Homogeneous steady-state\\
              \includegraphics[width=.435\linewidth]{figs/SIS_computation_times_Transient_expansion}& \includegraphics[width=.435\linewidth]{figs/SIS_computation_times_Steady-state_expansion}\\
              (c) Heterogeneous transient & (d) Heterogeneous steady-state 
       \end{tabular}
       \caption{Analysis of the computation time: for the transient regime, we compute $\bv(t)$ for $t\in[0,10]$. For the steady-state, we compute $\bv(\infty)$.}
       \label{fig:computationTime}
\end{figure}


\section*{Acknowledgments}

This work is supported by the French National Research Agency (ANR) through REFINO Project under Grant ANR-19-CE23-0015.  

     
\bibliographystyle{plain}
\bibliography{biblio,references}

\end{document}
