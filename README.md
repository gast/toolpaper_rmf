# toolpaper_rmf

The repository contains all files for the submission of the paper about rmf tool.

The notebook used to present the rmf tool at TOSME (Tools for Stochastic Modelling and Evaluation) workshop can be found in the subdirectory [notebook_TOSME_presentation ](https://gitlab.inria.fr/gast/toolpaper_rmf/-/tree/master/notebook_TOSME_presentation).
